# %% Import used packages
import pandas as pd
import numpy as np

import geopandas

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data
import global_parameter

# %% Load the data
data_folder = "data/LandKLIF/COMBINATION 2019-2021/MetaData/"
filename = "LandKLIF_bold_bin_meta_data"
df_LandKLIF = Load_Store_Data().loading(data_folder, filename, "parquet")

data_folder = "data/LFL_AUM/combination 19&20-neu/MetaData/"
filename = "LFL_AUM_bold_bin_meta_data"
df_LFL_AUM = Load_Store_Data().loading(data_folder, filename, "parquet")

year = "2019"
data_folder = f"data/LFU_AUM/{year}/MetaData/"
filename = "LFU_AUM_bold_bin_meta_data"
df_LFU_AUM_2019 = Load_Store_Data().loading(data_folder, filename, "parquet")

year = "2020"
data_folder = f"data/LFU_AUM/{year}/MetaData/"
df_LFU_AUM_2020 = Load_Store_Data().loading(data_folder, filename, "parquet")

year = "2021"
data_folder = f"data/LFU_AUM/{year}/MetaData/"
df_LFU_AUM_2021 = Load_Store_Data().loading(data_folder, filename, "parquet")

year = "2022"
data_folder = f"data/LFU_AUM/{year}/MetaData/"
df_LFU_AUM_2022 = Load_Store_Data().loading(data_folder, filename, "parquet")

data_folder = "data/LWF/MetaData/"
filename = "LWF_bold_bin_meta_data"
df_LWF = Load_Store_Data().loading(data_folder, filename, "parquet")

data_folder = "data/NatWald/MetaData/"
filename = "NatWald_bold_bin_meta_data"
df_NatWald = Load_Store_Data().loading(data_folder, filename, "parquet")

# %%
df_concat = pd.concat([df_LandKLIF,
                            df_LFL_AUM,
                            df_LFU_AUM_2019,
                            df_LFU_AUM_2020,
                            df_LFU_AUM_2021,
                            df_LFU_AUM_2022,
                            df_LWF,
                            df_NatWald],
                            ignore_index=True).reset_index(drop=True)#.astype("category")

df_concat["bold_bin_uri"] = df_concat["bold_bin_uri"].astype("string")

# %% Keep all BOLD BINs that have a good consensus score annotated

threshold_consensus_score =  global_parameter.consensus_score_threshold

mask = df_concat["consensus_score"].isin(threshold_consensus_score)
sum(mask)
#df_concat = df_concat[mask]

# %%
def set_consensus_grade_notna(X, consensus_grade):
    
    pos_consensus_grade = global_parameter.consensus_taxon_grades.index(consensus_grade)

    bold_bin_uri_singleton = X["bold_bin_uri"].value_counts()[X["bold_bin_uri"].value_counts() == 1].index
    df_singleton = X[X["bold_bin_uri"].isin(bold_bin_uri_singleton)]

    bold_bin_uri_duplicates = X["bold_bin_uri"].value_counts()[X["bold_bin_uri"].value_counts() > 1].index
    df_duplicates = X[X["bold_bin_uri"].isin(bold_bin_uri_duplicates)]

    mask_notna = df_duplicates[global_parameter.consensus_taxon_grades[:pos_consensus_grade+1]].notna()

    mask_notna_chains = (np.sum(mask_notna, axis=1) // (pos_consensus_grade + 1))
    mask_notna_chains = mask_notna_chains.astype(bool)

    df_duplicates_notna_chains = df_duplicates[mask_notna_chains]
    df_duplicates_short = df_duplicates_notna_chains.drop_duplicates("bold_bin_uri")

    return pd.concat([df_singleton, df_duplicates_short], axis = 0)

#df_concat = set_consensus_grade_notna(df_concat, "consensus_family")

# %% Store the data
data_folder = "data/UnifiedData/"
filename = "bold_bin_consensus_family_connection"
filename = "bold_bin_consensus_family_connection_unfiltered"

Load_Store_Data().saving(df_concat, data_folder, filename, "parquet")

# %% Load insect bin infos
data_folder = "data/MetaData/insect-trait-tool/"
filename = "insect-trait-tool-v1-0-2"

df_bin_infos = Load_Store_Data().loading(data_folder, filename, "csv", skiprows=1).dropna().astype("category")
df_bin_infos.rename({"family" : "consensus_family"}, axis=1, inplace=True)

# %% Store bin infos with bin and consensus family connection
data_folder = "data/UnifiedData/"

df_bin_infos_tutto = pd.DataFrame(df_concat
                                    .merge(df_bin_infos, on="consensus_family")).astype("category", copy=True)

df_bin_infos_tutto["bold_bin_uri"] = df_bin_infos_tutto["bold_bin_uri"].astype("string")

filename = "insect_trait_tool"
Load_Store_Data().saving(df_bin_infos, data_folder, filename, "parquet")

filename = "bold_bin_consensus_family_tutto"
filename = "bold_bin_consensus_family_tutto_unfiltered"
Load_Store_Data().saving(df_bin_infos_tutto, data_folder, filename, "parquet")
# %%

# %%
