# %% Import used packages
import pandas as pd
import numpy as np

# import geopandas
# import geopy.distance

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data
from global_parameter import timestamp_GTS_per_year, since_gts_intervall

# %% Load the data
data_folder = "data/LandKLIF/COMBINATION 2019-2021/"
filename = "Landklif_a_posto"
df_LandKLIF = Load_Store_Data().loading(data_folder, filename, "parquet")

data_folder = "data/LFL_AUM/combination 19&20-neu/"
filename = "LFL_19-20_a_posto"
df_LFL_AUM = Load_Store_Data().loading(data_folder, filename, "parquet")

year = "2019"
data_folder = f"data/LFU_AUM/{year}/"
filename = "LFU_AUM_a_posto"
df_LFU_AUM_2019 = Load_Store_Data().loading(data_folder, filename, "parquet")

year = "2020"
data_folder = f"data/LFU_AUM/{year}/"
df_LFU_AUM_2020 = Load_Store_Data().loading(data_folder, filename, "parquet")

year = "2021"
data_folder = f"data/LFU_AUM/{year}/"
df_LFU_AUM_2021 = Load_Store_Data().loading(data_folder, filename, "parquet")

year = "2022"
data_folder = f"data/LFU_AUM/{year}/"
df_LFU_AUM_2022= Load_Store_Data().loading(data_folder, filename, "parquet")

data_folder = "data/LWF/"
filename = "LFW_a_posto"
df_LWF = Load_Store_Data().loading(data_folder, filename, "parquet")

data_folder = "data/NatWald"
filename = "NatWald_a_posto"
df_NatWald = Load_Store_Data().loading(data_folder, filename, "parquet")

data_folder = "data/MetaData/Grünlandtemperatursumme/"
filename = "gruenlandtemperatursumme"
df_gts = Load_Store_Data().loading(data_folder, filename, "parquet")

# %%
df_concat = pd.concat([df_LandKLIF,
                            df_LFL_AUM,
                            df_LFU_AUM_2019,
                            df_LFU_AUM_2020,
                            df_LFU_AUM_2021,
                            df_LFU_AUM_2022,
                            df_LWF,
                            df_NatWald],
                            ignore_index=True)

# %% Split df_Concat into bin data (X) and target data (y)
target_headers = ["habitat_type", "timestamp", "trap_type", "latitude", "longitude", "dataset", "observation_id", "sum_raw_reads"]

df_target = df_concat[target_headers].copy(deep=True)
df_bins = df_concat.drop(target_headers, axis=1)

# %% pd.concat adds NaN which needs to be unified again
df_bins.fillna(0, inplace=True)

# %% pd.concat changes columns data type wich needs to be unified again
uint16_max_value = np.iinfo("uint16").max

if df_bins.max().max() <= np.iinfo("uint32").max:
    df_bins = df_bins.astype({key : "uint16" if df_bins[key].max() <= uint16_max_value else "uint32" for key in df_bins.columns}, copy=False)
else:
    print("OTC are too large for compressing data to uint16 or even uint32.")

df_bins = df_bins.copy(deep=True)

# %% pd.concat changes category columns to object wich needs to be unified again
df_target["dataset"] = df_target["dataset"].astype("category")

# %% pd.concat deals with empty trap types
df_target["trap_type"] = df_target["trap_type"].fillna("?").astype("category")

# %% Unify the habitat types
default_habitat_types = {
    "agriculture" :     "agriculture",
    "arable field" :    "agriculture",
    "urban" :           "urban",
    "settlement" :      "urban",
    "forest" :          "forest",
    "nature" :          "nature",
    "meadow" :          "nature",
    "grassland" :       "nature",

    "agriculture_ACO" : "agriculture_ACO",
    "agriculture_ACR" : "agriculture_ACR",
    "agriculture_edge" : "agriculture_edge",

    "forest_CF" : "forest_CF",
    "forest_edge" : "forest_edge",
    "forest_BL" : "forest_BL"
}

mask = df_target["habitat_type"].notna()
df_target.loc[mask, "habitat_type"] = [default_habitat_types[habitat] for habitat in df_target.loc[mask, "habitat_type"]]
df_target["habitat_type"] = df_target["habitat_type"].astype("category")

# %% Add months
df_target["month"] = [pd.to_datetime(datetime.month, format = "%m", errors = "coerce") for datetime in df_target["timestamp"]]

df_target["semimonth"] = [pd.to_datetime(f"01-{datetime.month}", format = "%d-%m", errors = "coerce") if datetime.day // 15 == 0 else 
                        pd.to_datetime(f"15-{datetime.month}", format = "%d-%m", errors = "coerce") for datetime in df_target["timestamp"]]

df_target.loc[df_target["timestamp"].isna(), "month"] = pd.NaT
df_target.loc[df_target["timestamp"].isna(), "semimonth"] = pd.NaT

target_headers += ["month", "semimonth"]

# %% Add days since GTS
get_nearest_wetherpoints = False

if get_nearest_wetherpoints:
    mask =  df_target["timestamp"].notna()
    df_target.loc[mask, "since_gts"] = df_target.loc[mask, "timestamp"].dt.year.astype(str)

    mask &= df_target["latitude"].notna()

    nearest_wetherpoints = pd.DataFrame(((lat, lon, *df_gts.loc[(abs(df_gts.loc[df_gts[year].notna(), "latitude"]-lat) + abs(df_gts.loc[df_gts[year].notna(), "longitude"]-lon)).idxmin(), ["location", "latitude", "longitude", year]])
                for (indx, (lat, lon, year)) in df_target.loc[mask, ["latitude", "longitude", "since_gts"]].drop_duplicates().iterrows()), columns=["latitude", "longitude", "location", "gts_latitude", "gts_longitude","gts_date"])
    nearest_wetherpoints["since_gts"] = nearest_wetherpoints["gts_date"].dt.year.astype(str)

    df_target["since_gts"] = pd.to_datetime(pd.merge(df_target[["latitude", "longitude", "since_gts"]], nearest_wetherpoints, 
                                        how="left", on=["latitude", "longitude", "since_gts"])["gts_date"])

    df_target["since_gts"] = (df_target.loc[mask, "timestamp"] - pd.to_datetime(df_target.loc[mask, "since_gts"])).dt.days

    mask =  df_target["timestamp"].notna() & df_target["latitude"].isna()
    if sum(mask):
        df_target.loc[mask, "since_gts"] = (df_target.loc[mask, "timestamp"] - df_target.loc[mask, "timestamp"].map(lambda x : timestamp_GTS_per_year[str(x.year)])).dt.days

else:
    mask =  df_target["timestamp"].notna()
    df_target.loc[mask, "since_gts"] = (df_target.loc[mask, "timestamp"] - df_target.loc[mask, "timestamp"].map(lambda x : timestamp_GTS_per_year[str(x.year)])).dt.days
    
target_headers += ["since_gts"]

# %% Change datatypes to category - but some exceptions remain
df_target = df_target.astype("category")
df_target["observation_id"] = df_target["observation_id"].astype("str")
df_target["sum_raw_reads"] = df_target["sum_raw_reads"].astype("int")
df_target["timestamp"] = pd.to_datetime(df_target["timestamp"])

# %% Get the target data that is non NaN
mask = ( df_target["habitat_type"].notna() 
        | df_target["timestamp"].notna()
        | df_target["latitude"].notna()
        | df_target["trap_type"].notna() )

# %% Ignore columns that have only any values > 0 in rows where the target data was completely NaN
bold_bin_uri_sum = np.sum(df_bins.loc[mask].astype("int"), axis=0)
bin_headers = [col for col in df_bins.columns if not col in list(bold_bin_uri_sum[bold_bin_uri_sum == 0].index)]

# %% Store the target data that is non NaN
data_folder = "data/UnifiedData/"
filename = "target_data_tutto"

Load_Store_Data().saving(df_target, data_folder, filename, "parquet")

# %% Store the BIN data that is non NaN
filename = "bin_data_tutto"

Load_Store_Data().saving(df_bins.loc[mask, bin_headers], data_folder, filename, "parquet")

# %%
list(df_target.columns)
# %%
df_bins[mask].info()

# %%
df_target[mask].info()

# %%
df_target["observation_id"].value_counts()[df_target["observation_id"].value_counts()>1]

# %%
df_target[df_bins["AAA2052"] > 0].groupby("dataset").count()
# %%

# datasets = ["LfL_AUM", "LfU_AUM_2019", "LfU_AUM_2020", "LfU_AUM_2021", "NatWald"]

# for dataset in datasets:
#     mask = y_raw["dataset"] == dataset    
    
#     data_folder = "data/TestData/SimulateTestCases"
#     filename = f"X_test_{dataset}"
#     Load_Store_Data().saving(X_raw[mask], data_folder, filename, "parquet")

#     filename = f"y_test_{dataset}"
#     Load_Store_Data().saving(y_raw[mask], data_folder, filename, "parquet")
    
# %%
T = df_target.groupby(["habitat_type", "dataset"]).count()
# %%
