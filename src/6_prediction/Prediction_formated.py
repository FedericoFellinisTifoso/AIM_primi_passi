# %%
import pandas as pd
import numpy as np

import joblib
 
import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data
from global_parameter import timestamp_GTS_per_year, since_gts_intervall

sys.path.append('../5_model/')
from train_model import Train_Model

import helper

from sklearn.metrics import confusion_matrix, classification_report

import seaborn as sns
import matplotlib.pyplot as plt
import plotly.express as plx

# %% Load the model
load_store_data = Load_Store_Data()
model = dict()

data_folder = "models"
filename = f"first_model"

y_target = "habitat_type"
model[y_target] = load_store_data.loading(f"{data_folder}/{y_target}/", f"{filename}_{y_target}", "joblib")

y_target = "month"
model[y_target] = load_store_data.loading(f"{data_folder}/{y_target}/", f"{filename}_{y_target}", "joblib")

y_target = "since_gts"
model[y_target] = load_store_data.loading(f"{data_folder}/{y_target}/", f"{filename}_{y_target}", "joblib")

# %%
y_target = ["habitat_type", "month", "since_gts"]
y_target = "month"

# %% Real test cases
print(f"real test cases - {y_target}")

data_folder = "data/TestData/TestCases/"
test_cases = ["Braunisch", "Eutropia", "Heibl", "Kaczmarek", "Lobinger"]
test_cases = ["Heibl"]

for test_case in test_cases:
    print(test_case)

    filename = f"{test_case}_a_posto"
    X_test_raw = load_store_data.loading(f"{data_folder}/{test_case}/", filename, "parquet")
    X_test_raw_since_gts = load_store_data.loading(f"{data_folder}/{test_case}/", filename, "parquet")
    
    X_test_raw[[col for col in model[y_target].feature_names_in_ if not col in X_test_raw]] = 0
    X_test_raw_since_gts[[col for col in model["since_gts"].feature_names_in_ if not col in X_test_raw_since_gts]] = 0
    
    X_test = X_test_raw[model[y_target].feature_names_in_].astype("bool")
    X_test_since_gts = X_test_raw_since_gts[model["since_gts"].feature_names_in_].astype("bool")

    y_test_raw = load_store_data.loading(f"{data_folder}/{test_case}/", filename, "parquet", columns = [y_target])
    y_test = y_test_raw // since_gts_intervall if y_target == "since_gts" else y_test_raw

    mask = y_test[y_target].notna()

    y_predict = pd.DataFrame(model[y_target].predict(X_test[mask]), columns=[y_target], dtype="category")
    y_predict_since_gts = pd.DataFrame(model["since_gts"].predict(X_test_since_gts[mask]), columns=["since_gts"], dtype="category")

    y_timestamp=load_store_data.loading(f"{data_folder}/{test_case}/", filename, "parquet", columns = ["timestamp"])
    y_predict_since_gts = pd.DataFrame((y_timestamp["timestamp"].map(lambda x : timestamp_GTS_per_year[str(x.year)]) + y_predict_since_gts["since_gts"].astype(int).map(lambda x :  pd.Timedelta(x*since_gts_intervall, "d"))), columns=["since_gts"])

    y_test = y_test[mask].astype("category")

    if y_target == "since_gts":
        helper.get_heatplot(y_test, y_predict, "prediction/TestCases/", f"heatplot_{test_case}_{y_target}_rfc",
            load_store_data.loading(f"{data_folder}/{test_case}/", filename, "parquet", columns = ["timestamp"]))
    else:
        helper.get_heatplot(y_test, y_predict, "prediction/TestCases/", f"heatplot_{test_case}_{y_target}_rfc")

# %%
helper.get_heatplot(y_predict, pd.y_predict.month, "prediction/TestCases/", f"heatplot_{test_case}_{y_target}_since_gts_rfc")
# %%
y_timestamp=load_store_data.loading(f"{data_folder}/{test_case}/", filename, "parquet", columns = ["timestamp"])
_y_predict = pd.DataFrame((y_timestamp["timestamp"].map(lambda x : timestamp_GTS_per_year[str(x.year)]) + y_predict[y_target].astype(int).map(lambda x :  pd.Timedelta(x*since_gts_intervall, "d"))), columns=[y_target])
_y_test = pd.DataFrame((y_timestamp["timestamp"].map(lambda x : timestamp_GTS_per_year[str(x.year)]) + y_test[y_target].astype(int).map(lambda x :  pd.Timedelta(x*since_gts_intervall, "d"))), columns=[y_target])

[f"{time.day}. {time.month_name()}" for time in 
    sorted(list(set(_y_test[y_target]).union(set(_y_predict[y_target]))))]


# %%
set(_y_test[y_target])
# %%
