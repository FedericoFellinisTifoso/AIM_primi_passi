# %% Import used packages
import pandas as pd
import numpy as np

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data
from global_parameter import timestamp_GTS_per_year, since_gts_intervall

from sklearn.metrics import confusion_matrix, classification_report

# import seaborn as sns
import matplotlib.pyplot as plt
import plotly.express as plx

# %%
load_store_data = Load_Store_Data()

data_folder_tutto = "data/UnifiedData/"
filename = "bold_bin_consenus_family_connection"
df_bin_consensus_family_connection= load_store_data.loading(data_folder_tutto, filename, "parquet")
        
# %% Get visualization of y_predict vs. y_test
def get_heatplot(_y_test, _y_predict, _data_folder, _filename, _y_timestamp = None): 

    y_target = _y_test.columns[0]
    
    if y_target == "habitat_type":
        ticks = sorted(list(set(_y_test[y_target]).union(set(_y_predict[y_target]))))
        
    elif y_target == "month":
        
        _y_test = pd.DataFrame(pd.to_datetime(_y_test[y_target], format = "%Y-%m-%d"), columns=[y_target])
        _y_predict = pd.DataFrame(pd.to_datetime(_y_predict[y_target], format = "%Y-%m-%d"), columns=[y_target])

        ticks = [time.month_name() for time in 
                    sorted(list(set(_y_test[y_target]).union(set(_y_predict[y_target]))))]
    
    elif y_target == "semimonth":

        _y_test = pd.DataFrame(pd.to_datetime(_y_test[y_target], format = "%Y-%m-%d"), columns=[y_target])
        _y_predict = pd.DataFrame(pd.to_datetime(_y_predict[y_target], format = "%Y-%m-%d"), columns=[y_target])

        ticks = [f"{time.day}. {time.month_name()}" for time in 
                    sorted(list(set(_y_test[y_target]).union(set(_y_predict[y_target]))))]

    elif y_target == "since_gts":
        if _y_timestamp is None:
            ticks = [f"{int(intervall * since_gts_intervall)} days" for intervall in 
                    sorted(list(set(_y_test[y_target]).union(set(_y_predict[y_target]))))]
        else:
            _y_predict = pd.DataFrame((_y_timestamp["timestamp"].map(lambda x : timestamp_GTS_per_year[str(x.year)]) + _y_predict[y_target].astype(int).map(lambda x :  pd.Timedelta(x*since_gts_intervall, "d"))), columns=[y_target])
            _y_test = pd.DataFrame((_y_timestamp["timestamp"].map(lambda x : timestamp_GTS_per_year[str(x.year)]) + _y_test[y_target].astype(int).map(lambda x :  pd.Timedelta(x*since_gts_intervall, "d"))), columns=[y_target])
            
            ticks = [f"{time.day}. {time.month_name()}" for time in 
                    sorted(list(set(_y_test[y_target]).union(set(_y_predict[y_target]))))]

    elif y_target == "latitude":
        ticks = [f"{lat}°" for lat in 
                    sorted(list(set(_y_test[y_target]).union(set(_y_predict[y_target]))))]


    cl_report = classification_report(_y_test, _y_predict, zero_division=0, target_names = ticks)
    print(cl_report)

    cl_report_csv = pd.DataFrame(classification_report(_y_test, _y_predict, output_dict=True, zero_division=0)).transpose().reset_index()

    conf_matrix = confusion_matrix(_y_test, _y_predict)

    plt.clf()
    fig = plx.imshow(conf_matrix, 
                    text_auto=True, 
                    color_continuous_scale='Magma',
                    x=[f"{tick}_p" for tick in ticks],
                    y=ticks
                    );

    load_store_data.saving(fig, _data_folder, _filename, "html")


# %% Predict data via df_test + model
def prediction_by_model(df_test, model, y_target, dummy_features = False):

    if str(type(model)) == "<class 'xgboost.sklearn.XGBClassifier'>":
        model_keys = model.get_booster().feature_names

    elif str(type(model)) =="<class 'sklearn.ensemble._forest.RandomForestClassifier'>":
        model_keys = model.feature_names_in_
        model_targets = model.classes_

    X_test_raw = pd.DataFrame(0, index=df_test.index, columns = model_keys)

    for col in df_test.keys():
            if col in model_keys:
                X_test_raw[col] = df_test[col]

    if dummy_features:
        X_test = X_test_raw.copy()
        X_test[X_test_raw > 0] = 1
    else:
        X_test = X_test_raw.divide(np.sum(X_test_raw, axis=1), axis = "index")

    y_predict = pd.DataFrame(model.predict(X_test), columns = [y_target], index = None)
    y_predict_proba = pd.DataFrame(model.predict_proba(X_test), index = None)
    
    return  y_predict, y_predict_proba

# %% Get model related columns
def match_X_columns_with_model(X, model_columns, scaling):
    
    X_temp = pd.DataFrame(0, columns = list(set(model_columns).difference(X)), index=X.index)
    X_temp[[col for col in X_temp.columns if "bias" in col]] = 1
    X_temp = pd.concat([X, X_temp], axis=1)

    X_temp = X_temp[model_columns]

    if scaling == "oneHot":    
        X_temp = X_temp.astype(bool) 
    
    elif scaling == "normalize":
        X_temp = (X_temp - X_temp.mean(axis=0)) / X_temp.std(axis=0) # 
        X_temp = X_temp.fillna(0)
        
    elif scaling == "fractionize":
        
        X_temp = X_temp.divide(np.sum(X_temp, axis=1), axis = "index")

    return X_temp

def get_X_consensus_families(X,):

    bold_bin_intersection = set(X.columns).intersection(set(df_bin_consensus_family_connection["bold_bin_uri"]))

    X_consensus_family = X.loc[:,bold_bin_intersection].rename({oldCol : newCol for oldCol, newCol in 
                        zip(df_bin_consensus_family_connection["bold_bin_uri"], df_bin_consensus_family_connection["consensus_family"])}, axis=1)
    return X_consensus_family.groupby(by=X_consensus_family.columns, axis=1).sum()

# %% Define model architecture in function
def get_compiled_small_model():
    small_model = tf.keras.Sequential([
      tf.keras.layers.Dense(32,kernel_initializer = 'uniform', activation='relu', input_dim = 12),
      tf.keras.layers.Dense(32,kernel_initializer = 'uniform', activation='relu'),
      tf.keras.layers.Dense(1,kernel_initializer = 'uniform')
    ])

    small_model.compile(optimizer=optimizer,
                  loss='mae',
                  metrics=['mse'])
    return small_model