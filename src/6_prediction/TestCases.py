# %% Import used packages
import pandas as pd
import numpy as np
import joblib
from xgboost import XGBClassifier
import plotly.express as plx

import sys
sys.path.append('../helper/')

import load_store_data
import helper

# %%

y_targets = ["habitat_type", "month", "latitude", "longitude"]

clf_models = dict()

for y_target in y_targets:

    model_name = "RandomForest"
    data_folder_model = f"models/{model_name}/"
    filename = f"first_model_{y_target}"
    filepath = os.path.expanduser(load_store_data.get_path(data_folder_model, filename, "joblib"))

    clf_models[y_target] = joblib.load(filepath)


y_targets = ["habitat_type"]#, "month"]#, "latitude", "longitude"]

xgb_models = dict()

for y_target in y_targets:

    model_name = "XGBoost"
    data_folder_model = f"models/{model_name}/"
    filename = f"first_model_{y_target}"
    filepath = os.path.expanduser(load_store_data.get_path(data_folder_model, filename, "json"))

    xgb_model = XGBClassifier()
    xgb_model.load_model(filepath)
    xgb_models[y_target] = xgb_model


print(f"XGB features: {len(xgb_models[y_target].get_booster().feature_names)}")

print(xgb_models[y_target])
# %% Load train datasets and check the bin intersection with test cases data

data_folder = "data/LandKLIF/COMBINATION 2019-2021/"
filename = "Landklif_a_posto"
df_LandKLIF = load_store_data.read_raw_parquet(data_folder, filename)

data_folder = "data/LFL_AUM/combination 19&20-neu/"
filename = "LFL_19-20_a_posto"
df_LFL_AUM = load_store_data.read_raw_parquet(data_folder, filename)

year = "2019"
data_folder = f"data/LFU_AUM/{year}/"
filename = "LFU_AUM_a_posto"
df_LFU_AUM_2019 = load_store_data.read_raw_parquet(data_folder, filename)

year = "2020"
data_folder = f"data/LFU_AUM/{year}/"
df_LFU_AUM_2020 = load_store_data.read_raw_parquet(data_folder, filename)

year = "2021"
data_folder = f"data/LFU_AUM/{year}/"
df_LFU_AUM_2021 = load_store_data.read_raw_parquet(data_folder, filename)

data_folder = "data/NatWald"
filename = "NatWald_a_posto"
df_NatWald = load_store_data.read_raw_parquet(data_folder, filename)

test_datasets = ["Braunisch", "Heibl", "Eutropia", "Kaczmarek", "Lobinger"]

datasets = ["LandKLIF", "LfL_AUM", "LfU_AUM_2019", "LfU_AUM_2020", "LfU_AUM_2020", "NatWald"]

all_columns = set(list(df_LandKLIF.columns) + list(df_LFL_AUM.columns) + list(df_LFU_AUM_2019.columns) + list(df_LFU_AUM_2020.columns) + list(df_LFU_AUM_2021.columns) + list(df_NatWald.columns))
for filename in test_datasets:

    data_folder = f"data/TestData/TestCases"

    df_test = load_store_data.read_raw_parquet(data_folder, filename)

    print(filename, df_test.shape)

    print(len([col for col in df_LandKLIF if col in df_test]), len([col for col in df_LandKLIF if col in df_test]) / df_test.shape[1])
    print(len([col for col in df_LFL_AUM if col in df_test]), len([col for col in df_LFL_AUM if col in df_test]) / df_test.shape[1])
    print(len([col for col in df_LFU_AUM_2019 if col in df_test]), len([col for col in df_LFU_AUM_2019 if col in df_test]) / df_test.shape[1])
    print(len([col for col in df_LFU_AUM_2020 if col in df_test]), len([col for col in df_LFU_AUM_2020 if col in df_test]) / df_test.shape[1])
    print(len([col for col in df_LFU_AUM_2021 if col in df_test]), len([col for col in df_LFU_AUM_2021 if col in df_test]) / df_test.shape[1])
    print(len([col for col in df_NatWald if col in df_test]), len([col for col in df_NatWald if col in df_test]) / df_test.shape[1])

    print()

    print(len([col for col in all_columns if col in df_test]), len([col for col in all_columns if col in df_test]) / df_test.shape[1])

    print()
# %% Load train datasets and check bin intersection beneath each of them

data_folder = "data/UnifiedData/"
filename = "complete_data_tutto"
#df_all = load_store_data.read_raw_parquet(data_folder, filename)

filename = "bin_data_tutto"
X_raw = load_store_data.read_raw_parquet(data_folder, filename)

filename = "target_data_tutto"
df_target = load_store_data.read_raw_parquet(data_folder, filename)

store_img_data_folder = f"plots/FeaturesDistribution/BinIntersection/"

sum_threshold = 0

habitat_types = ["agriculture", "forest", "nature", "urban"]
datasets = ["LandKLIF", "LfL_AUM", "LfU_AUM_2019", "LfU_AUM_2020", "LfU_AUM_2020", "NatWald"]

for habitat_type in habitat_types:
    mask_habitat = df_target["habitat_type"] == habitat_type
    bin_intersection = np.empty([len(datasets), len(datasets)])

    for i, first_dataset in enumerate(datasets):
        mask_first_df = df_target["dataset"] == first_dataset
        first_df=X_raw[mask_habitat & mask_first_df]
        
        first_df = np.sum(first_df, axis=0)[np.sum(first_df, axis=0) > sum_threshold]
        
        if len(first_df>0):
            for j, second_dataset in enumerate(datasets):
                mask_second_df = df_target["dataset"] == second_dataset


                second_df=X_raw[mask_habitat & mask_second_df]
                second_df = np.sum(second_df, axis=0)[np.sum(second_df, axis=0) > sum_threshold]
                
                if len(second_df) > 0:
                    bin_intersection[i,j] = len([col for col in first_df.index if col in second_df.index]) / len(first_df)
        
                else:
                    bin_intersection[i,j] = 0
        else:
            for j, second_dataset in enumerate(datasets):
                bin_intersection[i,j] = 0


    fig = plx.imshow(bin_intersection, 
                    text_auto=True, 
                    color_continuous_scale='Magma',
                    x=datasets,
                    y=datasets,
                    labels={"color" : "BOLD_BINS_intersection"}
                    );
        
    filename = f"heatplot_bin_intersections_{habitat_type}"
    load_store_data.write_img_to(fig, store_img_data_folder, filename, "html")

# %%
len([col for col in df_LFU_AUM_2020.keys() if col in df_LFU_AUM_2019.keys()])

# %%
1750 / len(df_LFU_AUM_2020.keys())
# %%

    print(filename, df_test.shape)

    [col for col in df_LandKLIF if col in df_test]), len([col for col in df_LandKLIF if col in df_test]) / df_test.shape[1])
    print(len([col for col in df_LFL_AUM if col in df_test]), len([col for col in df_LFL_AUM if col in df_test]) / df_test.shape[1])
    print(len([col for col in df_LFU_AUM_2019 if col in df_test]), len([col for col in df_LFU_AUM_2019 if col in df_test]) / df_test.shape[1])
    print(len([col for col in df_LFU_AUM_2020 if col in df_test]), len([col for col in df_LFU_AUM_2020 if col in df_test]) / df_test.shape[1])
    print(len([col for col in df_LFU_AUM_2021 if col in df_test]), len([col for col in df_LFU_AUM_2021 if col in df_test]) / df_test.shape[1])
    print(len([col for col in df_NatWald if col in df_test]), len([col for col in df_NatWald if col in df_test]) / df_test.shape[1])

    print()

    print(len([col for col in all_columns if col in df_test]), len([col for col in all_columns if col in df_test]) / df_test.shape[1])

    print()


# %% Load test datasets and check bin intersection with datasets

test_datasets = ["Braunisch", "Heibl", "Eutropia", "Kaczmarek", "Lobinger"]

for filename in test_datasets:

    data_folder = f"data/TestData/TestCases"

    df_test = load_store_data.read_raw_parquet(data_folder, filename)

    data_folder = f"prediction/TestCases/{model_name}/{filename}"
# %% Load test datasets and get predictions

data_folder_model = "models/RandomForest/"
filename = f"first_dummy_model_habitat_type"
filepath = os.path.expanduser(load_store_data.get_path(data_folder_model, filename, "joblib"))

clf_dummy_model = joblib.load(filepath)
model_name = "RandomForest"

test_datasets = ["Braunisch", "Heibl", "Eutropia", "Kaczmarek", "Lobinger"]

for filename in test_datasets:

    data_folder = f"data/TestData/TestCases"

    df_test = load_store_data.read_raw_parquet(data_folder, filename)

    data_folder = f"prediction/TestCases/{model_name}/{filename}"

    for y_target in y_targets:

        y_predict, y_predict_proba = helper.prediction_by_model(df_test, clf_dummy_model, y_target, dummy_features=True)
        
        path = load_store_data.get_path(data_folder, f"{y_target}_clf_dummy_predict", "csv")

        y_predict.set_index(df_test["extra_target"]).to_csv(path)

        path = load_store_data.get_path(data_folder, f"{y_target}_clf_dummy_predict_proba", "csv")
        y_predict_proba.rename(columns={oldCol : newCol for oldCol, newCol in zip(y_predict_proba.columns, clf_dummy_model.classes_ )}).set_index(df_test["extra_target"]).to_csv(path)


# %%
