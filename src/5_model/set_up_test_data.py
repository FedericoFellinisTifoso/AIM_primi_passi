# %% Load the packages
import pandas as pd
import numpy as np
import os

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data


# %% Load the data
data_folder = "data/UnifiedData/"
filename = "complete_data_tutto"
#df_all = Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "bin_data_tutto"
X_raw = Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "target_data_tutto"
df_target = Load_Store_Data().loading(data_folder, filename, "parquet")

# %%

y_target = "habitat_type"
datasets = ["LfL_AUM", "LfU_AUM_2019", "LfU_AUM_2020", "LfU_AUM_2021", "NatWald"]

load_store_data = Load_Store_Data()
for dataset in datasets:
    mask = df_target["dataset"] == dataset    
    
    data_folder = "data/TestData/SimulateTestCases"
    filename = f"X_test_{dataset}"
    load_store_data.saving(X_raw[mask], data_folder, filename, "parquet")

    filename = f"y_test_{dataset}"
    load_store_data.saving(df_target.loc[mask, y_target].to_frame(), data_folder, filename, "parquet")

# %%
