# %% Load the packages
import pandas as pd
import numpy as np
import os

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data
from global_parameter import timestamp_GTS_per_year, since_gts_intervall, consensus_score_threshold

from sklearn.model_selection import train_test_split
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor
from sklearn.model_selection import PredefinedSplit
from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import SMOTE

# %%
class Set_Up_Train_Data: 
    """Have all the train data wrangling and model fitting shizzle combined in one class
    """
    def __init__(self, cluster_subtypes = None):
        # %% Load the data
        self.load_store_data = Load_Store_Data()
        self.data_folder_tutto = "data/UnifiedData/"
        filename = "bin_data_tutto"
        self.X_raw = self.load_store_data.loading(self.data_folder_tutto, filename, "parquet")
        self.X_train = self.X_raw.copy()

        filename = "target_data_tutto"
        self.y_raw = self.load_store_data.loading(self.data_folder_tutto, filename, "parquet")
        self.y_train = self.y_raw.copy()

        filename = "bold_bin_consensus_family_connection"
        self.df_bin_consensus_family_connection= self.load_store_data.loading(self.data_folder_tutto, filename, "parquet")
        
        if cluster_subtypes:
            if "forest" in cluster_subtypes:
                self.y_raw.loc[self.y_raw["habitat_type"] == "forest_BL", "habitat_type"] = "forest"
                self.y_raw.loc[self.y_raw["habitat_type"] == "forest_CF", "habitat_type"] = "forest"
                self.y_raw.loc[self.y_raw["habitat_type"] == "forest_edge", "habitat_type"] = "forest"
            
            if "agriculture" in cluster_subtypes:
                self.y_raw.loc[self.y_raw["habitat_type"] == "agriculture_ACR", "habitat_type"] = "agriculture"
                self.y_raw.loc[self.y_raw["habitat_type"] == "agriculture_ACO", "habitat_type"] = "agriculture"
                self.y_raw.loc[self.y_raw["habitat_type"] == "agriculture_other", "habitat_type"] = "agriculture"

    def get_X_y_raw(self):

        return self.X_raw, self.y_raw

    def set_datasets_list(self, _datasets):

        self.datasets = _datasets

        return self

    def get_datasets_list(self):
        
        return self.datasets
    
    def set_mask_via_target(self, **kwargs):

        if kwargs:
            first_key = list(kwargs.keys())[0]
            self.mask = self.y_raw[first_key] == kwargs[first_key]
        else:
            self.mask = [True] * len(self.y_raw)
        
        return self

    def set_mask_via_target_AND(self, **kwargs):

        for key in kwargs:
            self.mask &= self.y_raw[key] == kwargs[key]

        return self
        
    def set_mask_via_target_OR(self, **kwargs):

        for key in kwargs:
            self.mask |= self.y_raw[key] == kwargs[key]

        return self

    def set_mask_on_train_data(self):

        self.X_train = self.X_raw[self.mask]
        self.y_train = self.y_raw[self.mask]

        return self

    def set_y_target(self, _target_label : str):

        self.y_target = _target_label

        if self.y_target in ["habitat_type", "timestamp", "month", "semimonth"]:
            mask = self.y_train[self.y_target].notna()
            self.y_train = self.y_train.loc[mask, [self.y_target]].astype(str)
            self.X_train = self.X_train.loc[mask]

        if self.y_target in ["since_gts"]:
            mask = self.y_train[self.y_target].notna()
            self.y_train = self.y_train.loc[mask, [self.y_target]] // since_gts_intervall
            self.X_train = self.X_train.loc[mask]

        elif self.y_target == "latitude":
            mask = self.y_train[self.y_target].notna()
            self.y_train = self.y_train.loc[mask, [self.y_target]].round(0).astype("int")
            self.X_train = self.X_train.loc[mask]

        elif self.y_target == "longitude":
            self.y_train = pd.DataFrame({"longitude" : self.y_train["longitude"].round(0).astype("int")}, columns = ["longitude"])
            print("Not enough longitude data for 6 -> change them to 7")
            mask = self.y_train["longitude"] == 6
            self.y_train.loc[mask, "longitude"] = 7

        return self

    def get_y_target(self):

        return self.y_target

    def get_X_y_train(self):

        return  self.X_train, self.y_train

    def reset_X_y_train(self):

        self.X_train = self.X_raw
        self.y_train = self.y_raw

        return  self

    def get_X_y_train(self):

        return  self.X_train, self.y_train

    def set_X_train_one_hot_encoding(self):

        self.X_train = self.X_train.astype("bool")

        return self

    def set_X_test_one_hot_encoding(self):

        self.X_test = self.X_test.astype("bool")

        return self

    def set_X_train_fractionize(self):

        self.X_train = self.X_train.divide(np.sum(self.X_train, axis=1), axis = "index")

        return self

    def set_X_test_fractionize(self):

        self.X_test = self.X_test.divide(np.sum(self.X_test, axis=1), axis = "index")

        return self

    def set_X_train_normalize(self):

        self.X_train = (self.X_train - self.X_train.mean(axis=0)) / self.X_train.std(axis=0)

        self.X_train = self.X_train.fillna(0)

        return self

    def set_X_test_normalize(self):

        self.X_test = (self.X_test - self.X_test.mean(axis=0)) / self.X_test.std(axis=0)

        self.X_test = self.X_test.fillna(0)

        return self

    def add_bias_node(self, bias_node_count : int = 1):
        
        bias =pd.DataFrame(1, index=np.arange(len(self.X_train)), columns=[f"bias_node_{i}" for i in range(bias_node_count)])

        self.X_train = pd.concat([self.X_train, bias], axis=1)
        
        return self

    def set_X_train_consensus_families(self):

        bold_bin_intersection = set(self.X_train.columns).intersection(set(self.df_bin_consensus_family_connection["bold_bin_uri"]))
        print(len(bold_bin_intersection))
        X_consensus_family = self.X_train.loc[:,bold_bin_intersection].rename({oldCol : newCol for oldCol, newCol in 
                            zip(self.df_bin_consensus_family_connection["bold_bin_uri"], self.df_bin_consensus_family_connection["consensus_family"])}, axis=1)
        print(X_consensus_family.shape)
        self.X_train = X_consensus_family.groupby(by=X_consensus_family.columns, axis=1).sum()

        return self

    def store_X_y_train(self, data_folder : str, _filename : str = None, _file_extension : str = "parquet", _project_folder : str = None, **kwargs):
        
        self.load_store_data.saving(self.X_train, data_folder, _filename if _filename else "X_train", _file_extension, _project_folder, **kwargs)
        self.load_store_data.saving(self.y_train, data_folder, _filename if _filename else "y_train", _file_extension, _project_folder, **kwargs)

        return self

    def store_X_y_test(self, data_folder : str, _filename : str = None, _file_extension : str = "parquet", _project_folder : str = None, **kwargs):
        
        self.load_store_data.saving(self.X_test, data_folder, _filename if _filename else "X_test", _file_extension, _project_folder, **kwargs)
        self.load_store_data.saving(self.y_test, data_folder, _filename if _filename else "y_test", _file_extension, _project_folder, **kwargs)

        return self
    
    def train_test_split(self, _test_size : float, _random_state : int = None):

        if _test_size > 0:
            
            self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(
                                                                        self.X_train, self.y_train, 
                                                                        test_size=_test_size, 
                                                                        stratify = self.y_train, 
                                                                        random_state=_random_state)

            self.y_test = pd.DataFrame(self.y_test[self.y_target].astype("category"))

        return self

    def set_X_train_threshold_consensus_score(self, _consensus_score_threshold = consensus_score_threshold):

        mask = self.df_bin_consensus_family_connection["consensus_score"].isin(_consensus_score_threshold)

        columns = list(set(self.X_train.columns).intersection(set(self.df_bin_consensus_family_connection.loc[mask, "bold_bin_uri"])))
        
        self.X_train = self.X_train[columns]

        return self

    def detect_outliers(self, _contamination = "auto", _n_neighbors : int = 2):

        # clf1 = AutoEncoder(hidden_neurons =[25, 25])
        # clf1.fit(self.X_train)
        # o = clf1.decision_function(self.X_train)

        #mask = IsolationForest(contamination=_contamination).fit_predict(self.X_train) != -1
        from pyod.models.auto_encoder import AutoEncoder
        clf1 = AutoEncoder(epochs=10)



        mask = len(self.X_train) * [False]
        for group in set(self.y_train[self.y_target]):
            mask_group = (self.y_train[self.y_target] == group) & self.y_train[self.y_target].notna()
            
            #mask_outliers = LocalOutlierFactor(n_neighbors=_n_neighbors).fit_predict(self.X_train[mask_group]) != -1
            #mask_outliers = IsolationForest(contamination=_contamination, n_estimators=100).fit_predict(self.X_train[mask_group]) != -1
            mask_outliers = clf1.fit(self.X_train[mask_group]).predict(self.X_train[mask_group]) != 1
            # j = 0
            # for i, val in enumerate(mask_group):
            #     if val:
            #         mask_group.iloc[i] = mask_outliers[j]
            #         j += 1
            
            mask |= mask_group
            #mask |= LocalOutlierFactor(n_neighbors=_n_neighbors).fit_predict(self.X_train[mask_group]) 
            
        self.X_train = self.X_train[mask]
        self.y_train = self.y_train[mask]

        return self

    def set_predefined_split(self,  **kwargs):

        if kwargs:
            first_key = list(kwargs.keys())[0]
            test_fold = self.y_raw[first_key] == kwargs[first_key]
        
            self.ps = PredefinedSplit(test_fold)

        return self

    def scale_X_train(self, _get_X_fraction_by : str = "sum_actual_reads"):

        nothing_scaled = "Unkown scaling command. Nothing was scaled."

        if _get_X_fraction_by == "sum_actual_reads":
            self.X_train = self.X_train.divide(np.sum(self.X_train, axis=1), axis = "index")

        elif get_X_fraction_by == "sum_raw_reads":
            self.X_train = self.X_train.divide(self.y_raw.loc[self.y_train.index, "sum_raw_reads"], axis = "index")

        else:
            print(nothing_scaled)

        return self

    def set_bin_reads_fraction_for_X_train(self, _min_bin_reads_fraction : float = 0.0, _max_bin_reads_fraction : float = 1.0):
        
        self.X_train = self.X_train[[col for col in self.X_train if _min_bin_reads_fraction <= np.sum(self.X_train[col]) <= _max_bin_reads_fraction]]

        return self

    def set_bin_reads_one_hot_for_X_train(self, _min_bin_reads_one_hot : int = 3, _max_bin_reads_one_hot : int = None):
        
        if _max_bin_reads_one_hot:
            self.X_train = self.X_train[[col for col in self.X_train if _min_bin_reads_one_hot <= np.sum(self.X_train[col]) <= _max_bin_reads_one_hot]]
        else:
            self.X_train = self.X_train[[col for col in self.X_train if _min_bin_reads_one_hot <= np.sum(self.X_train[col])]]

        return self

    def undersampling_X_y_train(self):

        value_counts = self.y_train[self.y_target].value_counts()
        meridian_value = value_counts.iloc[len(value_counts) // 2]
        reduce_majority_by = [ int((count - meridian_value) * 0.7) for count in value_counts.iloc[:len(value_counts) // 2]]
        
        sampling_strategy = {id: int(value_counts[id] - reduce_majority_by[i]) if i < len(reduce_majority_by) else value_counts[id] for i, id in enumerate(value_counts.index)}
        
        undersample = RandomUnderSampler(sampling_strategy=sampling_strategy)
        
        self.X_train, self.y_train = undersample.fit_resample(self.X_train, self.y_train[self.y_target])

        self.y_train = pd.DataFrame(self.y_train, columns = [self.y_target])

        return self

    def smote_X_y_train(self, _n_jobs : int = 5):
    
        oversample = SMOTE(n_jobs=_n_jobs)

        value_counts = self.y_train[self.y_target].value_counts()

        mask = self.y_train[self.y_target].isin(value_counts[value_counts >= oversample.get_params()["k_neighbors"]].index)
         
        self.X_train, self.y_train = oversample.fit_resample(self.X_train[mask], self.y_train[mask])

        return self