# %% Import used packages
import pandas as pd
import numpy as np

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

import helper

# %% Preprocess data

data_folder = "data/NatWald/"
filename = "Kuehbandner_NatWald100_pt1_results_JM"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

habitat_data = helper.preprocess_data(df_raw, 
                                        habitat_sub_header_pos_after_trace = 1)

habitat_data = helper.get_extra_target_data_hidden_behind_OTC_reads(df_raw, habitat_data,
                                                                        habitat_sub_header_pos_after_trace = 1)

Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata
consensus_score_sub_header_trace = "BIN sharing?"

bold_bin_metadata = helper.preprocess_metadata(df_raw, consensus_score_sub_header_trace = consensus_score_sub_header_trace)

data_folder += "MetaData/"
filename = "NatWald_bold_bin_meta_data"

Load_Store_Data().saving(bold_bin_metadata, data_folder, filename, "parquet")

# %%
len(df_raw.iloc[5:,0].str[:-3])
# %%
len(set(df_raw.iloc[5:,0].str[:-3]))

# %%
