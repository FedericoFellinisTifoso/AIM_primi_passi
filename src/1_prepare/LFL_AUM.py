# %% Import used packages
import pandas as pd
import numpy as np

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

import helper

# %% Preprocess data

data_folder = "data/LFL_AUM/combination 19&20-neu/"
filename = "LFL_19-20_results-combo092021_JM"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

habitat_data = helper.preprocess_data(df_raw)

Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata
bold_bin_meta_data = helper.preprocess_metadata(df_raw)

data_folder += "MetaData/"
filename = "LFL_AUM_bold_bin_meta_data"

Load_Store_Data().saving(bold_bin_meta_data, data_folder, filename, "parquet")