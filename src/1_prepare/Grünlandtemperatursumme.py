# %% Import used packages
import pandas as pd
import numpy as np

from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

import helper

# %% Preprocess data
def revise_gruenlandtemperaturesumme_data(_data_folder, _filename):
    with open(Load_Store_Data().get_path(_data_folder, _filename, "tsv"), 'r') as file:
        df_raw = file.readlines()

    with open(Load_Store_Data().get_path(_data_folder, f"{_filename}_revised", "csv"), 'w') as file:
        for i, line in enumerate(df_raw):
            if i % 2 == 0:
                file.write(f"{df_raw[i]}\t{df_raw[i+1]}".replace("\t\n\t", "\t").replace("\t", ";"))

# %%
data_folder = "data/MetaData/Grünlandtemperatursumme/"

df_raw = dict()

for year in range(2018, 2022):
    revise_gruenlandtemperaturesumme_data(data_folder, str(year))
    df_raw[str(year)] = Load_Store_Data().loading(data_folder, f"{year}_revised", "csv", header=None)
    df_raw[str(year)] = df_raw[str(year)].rename({0 : str(year), 1 : "location", 2 : "state"}, axis = 1).drop(columns=[3], axis=1)
    df_raw[str(year)][str(year)] = pd.to_datetime(df_raw[str(year)][str(year)] + str(year), format = "%d.%m.%Y")
        
# %%    
df_temp = pd.concat([df_raw[str(year)] for year in range(2018, 2022)], axis = 0)

df_gts = pd.DataFrame(set(df_temp["location"] + "_" + df_temp["state"]), columns = ["location"])

df_gts["state"] = df_gts["location"].str[-2:]
df_gts["location"] = df_gts["location"].str[:-3]
# %%
for year in range(2018, 2022):
    df_gts = pd.merge(df_gts, df_raw[str(year)][[str(year), "location"]], how = "outer", on = "location")

# %%

# %% Get Lon and Lat for geometry information via geopy
load_geolocation_data_from_Nominatim = False

if load_geolocation_data_from_Nominatim:

    geolocator = Nominatim(user_agent="name-of-your-user-agent")
    geolocator_delayed = RateLimiter(geolocator.geocode, min_delay_seconds=1)
    
    df_locations_metadata = df_gts[["location"]]

    df_locations_metadata["geolocator"] = df_locations_metadata["location"].apply(geolocator_delayed)

    df_locations_metadata["address"] = df_locations_metadata["geolocator"].apply(lambda x: x.address if x else None)
    df_locations_metadata["latitude"] = df_locations_metadata["geolocator"].apply(lambda x: x.latitude if x else None)
    df_locations_metadata["longitude"] = df_locations_metadata["geolocator"].apply(lambda x: x.longitude if x else None)

    df_gts["latitude"] = df_locations_metadata["latitude"]
    df_gts["longitude"] = df_locations_metadata["longitude"]

    filename = "locations_metadata"

    Load_Store_Data().saving(df_locations_metadata.drop(axis=1, columns=["geolocator"]), data_folder, filename, "csv")

else:
    filename = "locations_metadata"
    df_locations_metadata = Load_Store_Data().loading(data_folder, filename, "csv")

    df_gts = pd.merge(df_gts, df_locations_metadata[["location", "latitude", "longitude"]],  
                                    how='left', 
                                    on="location"
                                )

# %%
mask_gts = df_gts["latitude"].notna()

filename = "gruenlandtemperatursumme"
Load_Store_Data().saving(df_gts[mask_gts], data_folder, filename, "parquet")

# %%
df_gts.info()
# %%
