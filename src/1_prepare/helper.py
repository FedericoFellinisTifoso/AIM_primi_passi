# %% Import used packages
import pandas as pd
import numpy as np
import pyarrow
import dask.dataframe

# %% Split the data into BOLD BIN part and the habitat data
def preprocess_data(df_raw, sub_header_trace : str = "BOLD_Process_ID",
                    bold_bin_uri_sub_header_trace : str = "BOLD_BIN_uri",
                    habitat_sub_header_trace : str = "sum_reads_per_OTU",
                    habitat_sub_header_pos_after_trace : int = 0,
                    habitat_label_pos_after_trace : int = 1,
                    habitat_data_ending_trace : str = ""):

    # %% Find the row where the sub headers are stored
    sub_header_row = df_raw.index[df_raw.iloc[:,0] == sub_header_trace][0]

    # %% Find the column number where the BOLD_BIN_uri are stored
    bold_bin_uri_sub_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == bold_bin_uri_sub_header_trace][0])

    # %% Find the column number where the target headers are stored
    habitat_sub_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == habitat_sub_header_trace][0])
    habitat_sub_header_column += habitat_sub_header_pos_after_trace
    habitat_first_label_column = habitat_sub_header_column + habitat_label_pos_after_trace

    # %% Get the target headers
    target_headers = [df_raw.iloc[i, habitat_sub_header_column] for i in range(sub_header_row)]

    # %% Get BOLD_BIN_uri
    bold_bin_uri = df_raw.iloc[sub_header_row+1: , bold_bin_uri_sub_header_column]
    bold_bin_uri = bold_bin_uri.str.replace("BOLD:","")

    # %% Get the raw habitat data by checking columns that conclude sth. as target label or by getting explicitely ending column name
    if not habitat_data_ending_trace:
        habitat_target_label_mask = (~df_raw.loc[sub_header_row-1, :].isna()
                                    & [True if df_raw.columns.get_loc(col) >= habitat_first_label_column else False for col in df_raw])
    else:
        habitat_last_label_column = df_raw.columns.get_loc(habitat_data_ending_trace) - 1
        habitat_target_label_mask = [habitat_first_label_column <= df_raw.columns.get_loc(col) <= habitat_last_label_column for col in df_raw]

    habitat_data_raw = df_raw.loc[sub_header_row+1 : , habitat_target_label_mask].fillna(0).astype(int)

    # %% Transpose the raw habitat data and rename the columns with the BOLD_BIN_uri
    habitat_data_just_features = habitat_data_raw.T.reset_index(drop=True)
    habitat_data_just_features.rename(columns={old_col_name : new_col_name for old_col_name, new_col_name in zip(range(sub_header_row+1,len(df_raw)),bold_bin_uri)}, inplace=True)

    # %% Build the habitat data
    habitat_data = habitat_data_just_features.loc[:, habitat_data_just_features.columns.notnull()].copy()
    habitat_data = habitat_data.groupby(habitat_data.columns, axis=1).sum().copy()

    uint16_max_value = np.iinfo("uint16").max

    if habitat_data.max().max() <= np.iinfo("uint32").max:
        habitat_data = habitat_data.astype({key : "uint16" if habitat_data[key].max() <= uint16_max_value else "uint32" for key in habitat_data.columns}, copy=False)
        habitat_data = habitat_data.copy(deep=True)
    else:
        print("OTC are too large for compressing data to uint16 or even uint32.")

    # %% Add target labels
    for i, header in enumerate(target_headers):
        habitat_data[header] = df_raw.loc[i, habitat_target_label_mask].str.strip().to_list() # Transpose df_raw row into column doesn't work..
        habitat_data[header] = habitat_data[header].astype("category")

    # %% Add sum raw reads in sample
    habitat_data["sum_raw_reads"] = np.sum(habitat_data_raw, axis = 0).to_list() # Transpose df_raw row into column doesn't work..
        
    return habitat_data

# %% If special extra data needs to be extracted from the raw data
def get_extra_target_data_hidden_behind_OTC_reads(df_raw, df_target,
                                sub_header_trace : str = "BOLD_Process_ID",
                                habitat_sub_header_trace : str = "sum_reads_per_OTU",
                                habitat_sub_header_pos_after_trace : int = 0,
                                habitat_label_pos_after_trace : int = 1):

    # %% Find the row where the sub headers are stored
    sub_header_row = df_raw.index[df_raw.iloc[:,0] == sub_header_trace][0]

    # %% Find the column where the target headers are stored
    habitat_sub_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == habitat_sub_header_trace][0])
    habitat_sub_header_column += habitat_sub_header_pos_after_trace
    habitat_first_label_column = habitat_sub_header_column + habitat_label_pos_after_trace
    habitat_last_label_column = habitat_first_label_column + len(df_target)

    # %% Get the hidden extra data
    extra_target_data = df_raw.iloc[sub_header_row , habitat_first_label_column : habitat_last_label_column]

    # %% Add the hidden extra data
    df_target["extra_target"] = extra_target_data.str.strip().to_list()

    return df_target

# %% Get consensus taxons and connected BOLD BIN uri
def preprocess_metadata(df_raw, 
                    sub_header_trace : str = "BOLD_Process_ID",
                    bold_bin_uri_sub_header_trace : str = "BOLD_BIN_uri",
                    consensus_domain_sub_header_trace : str = "consensus_Domain",
                    consensus_phylum_sub_header_trace : str = "consensus_Phylum",
                    consensus_class_sub_header_trace : str = "consensus_Class",
                    consensus_order_sub_header_trace : str = "consensus_Order",
                    consensus_family_sub_header_trace : str = "consensus_Family",
                    consensus_genus_sub_header_trace : str = "consensus_Genus",
                    consensus_species_sub_header_trace : str = "consensus_Species",
                    consensus_score_sub_header_trace : str = "consensus score"):

    # %% Find the row where the sub headers are stored
    sub_header_row = df_raw.index[df_raw.iloc[:,0] == sub_header_trace][0]

    # %% Find the column where the BOLD_BIN_uri are stored
    bold_bin_uri_sub_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == bold_bin_uri_sub_header_trace][0])

    # %% Find the column where the consenus family names are stored
    consensus_domain_sub_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == consensus_domain_sub_header_trace][0])
    consensus_phylum_sub_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == consensus_phylum_sub_header_trace][0])
    consensus_class_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == consensus_class_sub_header_trace][0])
    consensus_order_sub_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == consensus_order_sub_header_trace][0])
    consenus_family_sub_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == consensus_family_sub_header_trace][0])
    consensus_genus_sub_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == consensus_genus_sub_header_trace][0])
    consensus_species_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == consensus_species_sub_header_trace][0])

    # %% Find the column number where the consensus scores are stored
    consensus_score_sub_header_column = df_raw.columns.get_loc(df_raw.columns[df_raw.iloc[sub_header_row, :] == consensus_score_sub_header_trace][0])
   
    # %% Get BOLD_BIN_uri
    bold_bin_uri = df_raw.iloc[sub_header_row+1: , bold_bin_uri_sub_header_column]
    bold_bin_uri = bold_bin_uri.str.replace("BOLD:","")

    # %% Get consensus family names
    consensus_domain = df_raw.iloc[sub_header_row+1: , consensus_domain_sub_header_column]
    consensus_phylum = df_raw.iloc[sub_header_row+1: , consensus_phylum_sub_header_column]
    consensus_class = df_raw.iloc[sub_header_row+1: , consensus_class_header_column]
    consensus_order = df_raw.iloc[sub_header_row+1: , consensus_order_sub_header_column]
    consensus_family = df_raw.iloc[sub_header_row+1: , consenus_family_sub_header_column]
    consensus_genus = df_raw.iloc[sub_header_row+1: , consensus_genus_sub_header_column]
    consensus_species = df_raw.iloc[sub_header_row+1: , consensus_species_header_column]

    # %% Get consenus score
    consensus_score = df_raw.iloc[sub_header_row+1: , consensus_score_sub_header_column]
        
    # %% Connect the data
    metadata_dict = {"bold_bin_uri" : bold_bin_uri, 
                        "consensus_domain" : consensus_domain,
                        "consensus_phylum" : consensus_phylum,
                        "consensus_class" : consensus_class,
                        "consensus_order" : consensus_order,
                        "consensus_family" : consensus_family,
                        "consensus_genus" : consensus_genus,
                        "consensus_species" : consensus_species,
                        "consensus_score" : consensus_score}

    return pd.DataFrame(metadata_dict, dtype="category").dropna(subset=["bold_bin_uri"]).drop_duplicates()
