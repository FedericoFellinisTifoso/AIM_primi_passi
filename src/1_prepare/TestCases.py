# %% Import used packages
import pandas as pd
import numpy as np

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

import helper

# %% Preprocess data

data_folder = "data/TestData/TestCases/Braunisch/"
filename = "Braunisch_446-1590-5-2019_results_JM"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

sub_header_trace = "processid"
bold_bin_uri_sub_header_trace = "bin_uri"
habitat_sub_header_trace = "sum_reads_per_OTU"
habitat_sub_header_col_after_trace = 0
habitat_label_pos_after_trace = 1
habitat_data_ending_trace = "BOLD database – February 2020 - metadata"

habitat_data = helper.preprocess_data(df_raw, 
                                sub_header_trace,
                                bold_bin_uri_sub_header_trace,
                                habitat_sub_header_trace,
                                habitat_sub_header_col_after_trace,
                                habitat_label_pos_after_trace,
                                habitat_data_ending_trace)

habitat_data = helper.get_extra_target_data_hidden_behind_OTC_reads(df_raw, habitat_data,
                                sub_header_trace,                                
                                habitat_sub_header_trace,
                                habitat_sub_header_col_after_trace,
                                habitat_label_pos_after_trace)

filename = "Braunisch"
Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata

consensus_family_sub_header_trace = "family_name" # BOLD Database

bold_bin_consensus_family = helper.preprocess_metadata(df_raw, 
                                            sub_header_trace,
                                            bold_bin_uri_sub_header_trace,
                                            consensus_family_sub_header_trace)

data_folder += "MetaData/"
filename += "_bold_bin_consensus_family"
Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess data

data_folder = "data/TestData/TestCases/Heibl/"
filename = "cons_Heibl_NGSeco15112018_results"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

sub_header_trace = "BOLD_Process_ID"
bold_bin_uri_sub_header_trace = "BOLD_BIN_uri"
habitat_sub_header_trace = "sum_reads_per_OTU"
habitat_sub_header_col_after_trace = 0
habitat_label_pos_after_trace = 1
habitat_data_ending_trace = "NCBI Genbank nt database BLAST – grade-%-ID-adjusted taxonomy (Feb 2020)"

habitat_data = helper.preprocess_data(df_raw, 
                                sub_header_trace,
                                bold_bin_uri_sub_header_trace,
                                habitat_sub_header_trace,
                                habitat_sub_header_col_after_trace,
                                habitat_label_pos_after_trace,
                                habitat_data_ending_trace)

habitat_data = helper.get_extra_target_data_hidden_behind_OTC_reads(df_raw, habitat_data,
                                sub_header_trace,                                
                                habitat_sub_header_trace,
                                habitat_sub_header_col_after_trace,
                                habitat_label_pos_after_trace)

filename = "Heibl"
Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata

consensus_family_sub_header_trace = "consensus_Family" 

bold_bin_consensus_family = helper.preprocess_metadata(df_raw, 
                                            sub_header_trace,
                                            bold_bin_uri_sub_header_trace,
                                            consensus_family_sub_header_trace)

data_folder += "MetaData/"
filename += "_bold_bin_consensus_family"
Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")


# %% Preprocess data

data_folder = "data/TestData/TestCases/Eutropia/"
filename = "Eutropia_20200929NGSeco-C_results"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

sub_header_trace = "BOLD_Process_ID"
bold_bin_uri_sub_header_trace = "BOLD_BIN_uri"
habitat_sub_header_trace = "sum_reads_per_OTU"
habitat_sub_header_col_after_trace = 0
habitat_label_pos_after_trace = 1
habitat_data_ending_trace = "NCBI Genbank nt database BLAST – grade-%-ID-adjusted taxonomy (Feb 2020)"

habitat_data = helper.preprocess_data(df_raw, 
                                sub_header_trace,
                                bold_bin_uri_sub_header_trace,
                                habitat_sub_header_trace,
                                habitat_sub_header_col_after_trace,
                                habitat_label_pos_after_trace,
                                habitat_data_ending_trace)

habitat_data = helper.get_extra_target_data_hidden_behind_OTC_reads(df_raw, habitat_data,
                                sub_header_trace,                                
                                habitat_sub_header_trace,
                                habitat_sub_header_col_after_trace,
                                habitat_label_pos_after_trace)

filename = "Eutropia"
Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata

consensus_family_sub_header_trace = "consensus_Family" 

bold_bin_consensus_family = helper.preprocess_metadata(df_raw, 
                                            sub_header_trace,
                                            bold_bin_uri_sub_header_trace,
                                            consensus_family_sub_header_trace)

data_folder += "MetaData/"
filename += "_bold_bin_consensus_family"
Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")


# %% Preprocess data

data_folder = "data/TestData/TestCases/Kaczmarek/"
filename = "Kaczmarek_BI_2021_004_results"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

sub_header_trace = "BOLD_Process_ID"
bold_bin_uri_sub_header_trace = "BOLD_BIN_uri"
habitat_sub_header_trace = "sum_reads_per_OTU"
habitat_sub_header_col_after_trace = 0
habitat_label_pos_after_trace = 1

habitat_data = helper.preprocess_data(df_raw, 
                                sub_header_trace,
                                bold_bin_uri_sub_header_trace,
                                habitat_sub_header_trace,
                                habitat_sub_header_col_after_trace,
                                habitat_label_pos_after_trace)

# Target row has no name given
habitat_data["customer_sampleID"] = habitat_data[float("nan")]
habitat_data.drop(columns=[float("nan")], inplace=True)

habitat_data = helper.get_extra_target_data_hidden_behind_OTC_reads(df_raw, habitat_data,
                                sub_header_trace,                                
                                habitat_sub_header_trace,
                                habitat_sub_header_col_after_trace,
                                habitat_label_pos_after_trace)

filename = "Kaczmarek"
Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata

consensus_family_sub_header_trace = "consensus_Family" 

bold_bin_consensus_family = helper.preprocess_metadata(df_raw, 
                                            sub_header_trace,
                                            bold_bin_uri_sub_header_trace,
                                            consensus_family_sub_header_trace)

data_folder += "MetaData/"
filename += "_bold_bin_consensus_family"
Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")


# %% Preprocess data

data_folder = "data/TestData/TestCases/Lobinger/"
filename = "Lobinger_EC_2021_159_COI_results"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

sub_header_trace = "BOLD_Process_ID"
bold_bin_uri_sub_header_trace = "BOLD_BIN_uri"
habitat_sub_header_trace = "sum_reads_per_OTU"
habitat_sub_header_col_after_trace = 0
habitat_label_pos_after_trace = 1
habitat_data_ending_trace = "NCBI Genbank nt database BLAST – grade-%-ID-adjusted taxonomy (Feb 2020)"

habitat_data = helper.preprocess_data(df_raw, 
                                sub_header_trace,
                                bold_bin_uri_sub_header_trace,
                                habitat_sub_header_trace,
                                habitat_sub_header_col_after_trace,
                                habitat_label_pos_after_trace,
                                habitat_data_ending_trace)

habitat_data = helper.get_extra_target_data_hidden_behind_OTC_reads(df_raw, habitat_data,
                                sub_header_trace,                                
                                habitat_sub_header_trace,
                                habitat_sub_header_col_after_trace,
                                habitat_label_pos_after_trace)

filename = "Lobinger"
Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata

consensus_family_sub_header_trace = "consensus_Family" 

bold_bin_consensus_family = helper.preprocess_metadata(df_raw, 
                                            sub_header_trace,
                                            bold_bin_uri_sub_header_trace,
                                            consensus_family_sub_header_trace)

data_folder += "MetaData/"
filename += "_bold_bin_consensus_family"
Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %%
