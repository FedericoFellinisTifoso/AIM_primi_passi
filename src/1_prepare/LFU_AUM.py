# %% Import used packages
import pandas as pd
import numpy as np

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

import helper

# %% Preprocess data meaning cutting out the OTC counts for each BIN along every observation

year = "2019"
data_folder = f"data/LFU_AUM/{year}/"
filename = "LFU_AUM_combined_CE_results"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

sub_header_trace = "processid"
bold_bin_uri_sub_header_trace = "bin_uri"
habitat_sub_header_trace = "sum_reads_per_OTU"
habitat_sub_header_pos_after_trace = 0
habitat_label_pos_after_trace = 9

habitat_data = helper.preprocess_data(df_raw, 
                                 sub_header_trace = sub_header_trace,
                                 bold_bin_uri_sub_header_trace = bold_bin_uri_sub_header_trace,
                                 habitat_sub_header_pos_after_trace = habitat_sub_header_pos_after_trace,
                                 habitat_label_pos_after_trace = habitat_label_pos_after_trace)

habitat_data = helper.get_extra_target_data_hidden_behind_OTC_reads(df_raw, habitat_data,
                                 sub_header_trace = sub_header_trace,
                                 habitat_sub_header_pos_after_trace = habitat_sub_header_pos_after_trace,
                                 habitat_label_pos_after_trace = habitat_label_pos_after_trace)

Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")


# %% Preprocess metadata

consensus_domain_sub_header_trace = "RDP_Domain"
consensus_phylum_sub_header_trace = "phylum_name"
consensus_class_sub_header_trace = "class_name"
consensus_order_sub_header_trace = "order_name"
consensus_family_sub_header_trace = "family_name"
consensus_genus_sub_header_trace = "genus_name"
consensus_species_sub_header_trace = "species_name"
consensus_score_sub_header_trace = "BIN sharing?"


bold_bin_meta_data = helper.preprocess_metadata(df_raw, 
                                             sub_header_trace = sub_header_trace,
                                             bold_bin_uri_sub_header_trace = bold_bin_uri_sub_header_trace,
                                             consensus_domain_sub_header_trace = consensus_domain_sub_header_trace,
                                             consensus_phylum_sub_header_trace = consensus_phylum_sub_header_trace,
                                             consensus_class_sub_header_trace = consensus_class_sub_header_trace,
                                             consensus_order_sub_header_trace = consensus_order_sub_header_trace,
                                             consensus_family_sub_header_trace = consensus_family_sub_header_trace,
                                             consensus_genus_sub_header_trace = consensus_genus_sub_header_trace,
                                             consensus_species_sub_header_trace = consensus_species_sub_header_trace,
                                             consensus_score_sub_header_trace = consensus_score_sub_header_trace)

data_folder += "MetaData/"
filename = "LFU_AUM_bold_bin_meta_data"
Load_Store_Data().saving(bold_bin_meta_data, data_folder, filename, "parquet")

# %%
year = "2020"
data_folder = f"data/LFU_AUM/{year}/"
filename = "LFU_AUM_2020"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

sub_header_trace = "BOLD_Process_ID"
bold_bin_uri_sub_header_trace = "BOLD_BIN_uri"
habitat_sub_header_trace = "sum_reads_per_OTU"
habitat_sub_header_col_after_trace = 0
habitat_label_pos_after_trace = 1

habitat_data = helper.preprocess_data(df_raw)

Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata
consensus_score_sub_header_trace = "BIN sharing?"

bold_bin_meta_data = helper.preprocess_metadata(df_raw, consensus_score_sub_header_trace = consensus_score_sub_header_trace)

data_folder += "MetaData/"
filename = "LFU_AUM_bold_bin_meta_data"
Load_Store_Data().saving(bold_bin_meta_data, data_folder, filename, "parquet")

# %%
year = "2021"
data_folder = f"data/LFU_AUM/{year}/"
filename = "FT_Foeldesi_LFU_AUM_2021_pt3_results"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

habitat_data = helper.preprocess_data(df_raw)

Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata
consensus_score_sub_header_trace = "BIN sharing?"

bold_bin_meta_data = helper.preprocess_metadata(df_raw, consensus_score_sub_header_trace = consensus_score_sub_header_trace)

data_folder += "MetaData/"
filename = "LFU_AUM_bold_bin_meta_data"
Load_Store_Data().saving(bold_bin_meta_data, data_folder, filename, "parquet")

# %%
year = "2021"
data_folder = f"data/LFU_AUM/{year}/"
filename = "LFU_AUM_KF_results"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

habitat_data = helper.preprocess_data(df_raw)

Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata

bold_bin_meta_data = pd.concat([bold_bin_meta_data, helper.preprocess_metadata(df_raw)])

data_folder += "MetaData/"
filename = "LFU_AUM_bold_bin_meta_data"
Load_Store_Data().saving(bold_bin_meta_data, data_folder, filename, "parquet")

# %%
# %%
year = "2022"
data_folder = f"data/LFU_AUM/{year}/"
filename = "LFU_AUM_custom_2022_results_JM3"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

habitat_data = helper.preprocess_data(df_raw,
                                habitat_data_ending_trace = "NCBI Genbank nt database BLAST – grade-%-ID-adjusted taxonomy (Feb 2020)")

habitat_data = helper.get_extra_target_data_hidden_behind_OTC_reads(df_raw, habitat_data)

Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata
bold_bin_meta_data = helper.preprocess_metadata(df_raw)

data_folder += "MetaData/"
filename = "LFU_AUM_bold_bin_meta_data"
Load_Store_Data().saving(bold_bin_meta_data, data_folder, filename, "parquet")

# %%
