# %% Import used packages
import pandas as pd
import numpy as np

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

import helper

# %% Preprocess data

data_folder = "data/LWF/"
filename = "Schauer_LWF_results"

df_raw = Load_Store_Data().loading(data_folder, filename, "csv")

habitat_data = helper.preprocess_data(df_raw)

# Target row has no name given
habitat_data["customer_sampleID"] = habitat_data[float("nan")]
habitat_data.drop(columns=[float("nan")], inplace=True)

Load_Store_Data().saving(habitat_data, data_folder, filename, "parquet")

# %% Preprocess metadata
bold_bin_meta_data = helper.preprocess_metadata(df_raw)

data_folder += "MetaData/"
filename = "LWF_bold_bin_meta_data"

Load_Store_Data().saving(bold_bin_meta_data, data_folder, filename, "parquet")

# %%
bold_bin_meta_data.loc[bold_bin_meta_data["bold_bin_uri"] == "AAE6029"]
# %%
habitat_data.info()
# %%
