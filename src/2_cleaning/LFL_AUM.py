# %% Import used packages
import pandas as pd
import numpy as np

from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data


# %% Load the data
data_folder = "data/LFL_AUM/combination 19&20-neu/"
filename = "LFL_19-20_results-combo092021_JM"
df_LFL_AUM = Load_Store_Data().loading(data_folder, filename, "parquet")

# %% Insert habitat type
df_LFL_AUM["habitat_type"] = pd.Series(["agriculture"] * len(df_LFL_AUM), dtype="category")
df_LFL_AUM["customer_sampleID"]
# %% Add timestamp to LFL 
df_LFL_AUM["timestamp"] = [pd.to_datetime(val, format = "%d.%m.%y", errors = "coerce", exact=False) for val in df_LFL_AUM["customer_sampleID"]]

mask = (df_LFL_AUM["timestamp"].isna() 
            & df_LFL_AUM["customer_sampleID"].str.contains("Bayerischer Wald") 
            & df_LFL_AUM["customer_sampleID"].str.contains("Zeitraum1")
            )
df_LFL_AUM.loc[mask, "timestamp"] = pd.to_datetime("2021/06/18")

# %% Generate temporarily dataframe for faster target header operations
df_LFL_AUM_temp = pd.DataFrame()
df_LFL_AUM_temp["customer_sampleID"] = df_LFL_AUM[["customer_sampleID"]]
df_LFL_AUM_temp["timestamp"] = df_LFL_AUM[["timestamp"]]

sampleID_splitted = df_LFL_AUM_temp["customer_sampleID"].str.split("-")

# %% Get Lon and Lat for geometry information via geopy
load_geolocation_data_from_Nominatim = False

if load_geolocation_data_from_Nominatim:

    geolocator = Nominatim(user_agent="name-of-your-user-agent")
    geolocator_delayed = RateLimiter(geolocator.geocode, min_delay_seconds=1)

    df_locations_metadata = pd.DataFrame([], columns=["Geo_ID", "address", "latitude", "longitude"])

    for i in range(sampleID_splitted.str.len().max()):
        sampleID_splitted_parte_i = np.array([part[i] if len(part)>i else 0 for part in sampleID_splitted])

        mask = [sample.replace(" ", "").isalpha() for sample in sampleID_splitted_parte_i]
        if i > 0:
            mask &= df_LFL_AUM_temp["Geo_ID"].isna()

        df_LFL_AUM_temp.loc[mask, "Geo_ID"] = sampleID_splitted_parte_i[mask]

        geo_id_to_search = list(set(df_LFL_AUM_temp.loc[mask, "Geo_ID"]))
        geolocation_found = [geolocator_delayed(loc) for loc in geo_id_to_search]

        for j in range(len(geolocation_found)):
            loc = geolocation_found[j]
            if loc and "Deutschland" in loc.address:
                df_locations_metadata.loc[len(df_locations_metadata)] = [geo_id_to_search[j], loc.address, loc.latitude, loc.longitude]
        
        mask =  ~ df_LFL_AUM_temp["Geo_ID"].isin(df_locations_metadata["Geo_ID"])
        df_LFL_AUM_temp.loc[mask, "Geo_ID"] = None

    data_folder += "MetaData/"
    filename = "LfL_AUM_geolocation"

    Load_Store_Data().saving(df_locations_metadata, data_folder, filename, "csv")

else:
        data_folder += "MetaData/"
        filename = "LfL_AUM_geolocation_revised_kl"

        df_locations_metadata = Load_Store_Data().loading(data_folder, filename, "csv")

        for i in range(sampleID_splitted.str.len().max()):
            sampleID_splitted_parte_i = np.array([part[i] if len(part)>i else 0 for part in sampleID_splitted])

            mask = [sample.replace(" ", "").isalpha() for sample in sampleID_splitted_parte_i]
            if i > 0:
                mask &= df_LFL_AUM_temp["Geo_ID"].isna()

            df_LFL_AUM_temp.loc[mask, "Geo_ID"] = sampleID_splitted_parte_i[mask]
            
            mask =  ~ df_LFL_AUM_temp["Geo_ID"].isin(df_locations_metadata["Geo_ID"])
            df_LFL_AUM_temp.loc[mask, "Geo_ID"] = None


# %%  Deal with strange formatted customer_sampleID where it is difficult to extract Geo_ID from

known_hidden_geo_id = ["Ostallgäu"]

mask = df_LFL_AUM_temp["Geo_ID"].isna()
for geo_id in known_hidden_geo_id:
    df_LFL_AUM_temp.loc[mask, "Geo_ID"] =  [geo_id if geo_id in sampleID else None for sampleID in df_LFL_AUM_temp.loc[mask, "customer_sampleID"] ]
    mask = df_LFL_AUM_temp["Geo_ID"].isna()

# %% Load NatWald location metadata
df_LFL_AUM_temp_merge = pd.merge(df_LFL_AUM_temp, df_locations_metadata[["Geo_ID", "latitude", "longitude"]],  
                                    how='left', 
                                    on=["Geo_ID"]
                                ).reset_index(drop=True)

# %% Use Lon and Lat for geometry information

df_LFL_AUM["latitude"] = df_LFL_AUM_temp_merge["latitude"].astype("category")
df_LFL_AUM["longitude"] = df_LFL_AUM_temp_merge["longitude"].astype("category")

# df_LFL_AUM = geopandas.GeoDataFrame(df_LFL_AUM, 
#                                         geometry = geopandas.points_from_xy(df_LFL_AUM_temp_merged["longitude"], 
#                                                                             df_LFL_AUM_temp_merged["latitude"]))
# %% Use customer_sampleID as observation_id
df_LFL_AUM["dataset"] = "LfL_AUM"
df_LFL_AUM["dataset"] = df_LFL_AUM["dataset"].astype("category", copy=False)

df_LFL_AUM["observation_id"] = df_LFL_AUM["customer_sampleID"]

# %% Remove customer_sampleID now that all data is extracted from it
df_LFL_AUM.drop(columns=["customer_sampleID"], inplace = True)

# %% Store the cleaned data
data_folder = "data/LFL_AUM/combination 19&20-neu/"
filename = "LFL_19-20_a_posto"
Load_Store_Data().saving(df_LFL_AUM, data_folder, filename, "parquet")

# %%
df_LFL_AUM["timestamp"].value_counts()
# %%
