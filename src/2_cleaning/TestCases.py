# %% Import used packages
import pandas as pd
import numpy as np

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data
from global_parameter import timestamp_GTS_per_year

# %% Load the data
data_folder = "data/TestData/TestCases/Heibl/"
filename = "Heibl"
df_Heibl = Load_Store_Data().loading(data_folder, filename, "parquet")

# %% Insert habitat type


# %% Add timestamp to Heibl
df_Heibl["timestamp"] = [pd.to_datetime(value, format = "%d_%m_%Y", errors = "coerce", exact=False) for value in df_Heibl["extra_target"]]

# %% Add months
df_Heibl["month"] = [pd.to_datetime(datetime.month, format = "%m", errors = "coerce") for datetime in df_Heibl["timestamp"]]
df_Heibl["semimonth"] = [pd.to_datetime(f"01-{datetime.month}", format = "%d-%m", errors = "coerce") if datetime.day // 15 == 0 else 
                        pd.to_datetime(f"15-{datetime.month}", format = "%d-%m", errors = "coerce") for datetime in df_Heibl["timestamp"]]

df_Heibl.loc[df_Heibl["timestamp"].isna(), "month"] = pd.NaT
df_Heibl.loc[df_Heibl["timestamp"].isna(), "semimonth"] = pd.NaT

# %% Add days since GTS
mask =  df_Heibl["timestamp"].notna()
df_Heibl.loc[mask, "since_gts"] = (df_Heibl.loc[mask, "timestamp"] - df_Heibl.loc[mask, "timestamp"].map(lambda x : timestamp_GTS_per_year[str(x.year)])).dt.days

# %% Use extra_target as observation_id
df_Heibl["dataset"] = "Heibl"
df_Heibl["observation_id"] = df_Heibl["extra_target"]

# %% Remove customer_sampleID now that all data is extracted from it
df_Heibl.drop(columns=["extra_target"], inplace = True)

# %% Store the cleaned data
filename = "Heibl_a_posto"
Load_Store_Data().saving(df_Heibl, data_folder, filename, "parquet")

# %%
# %%
X = pd.DataFrame(Load_Store_Data().loading(data_folder, filename, "parquet"))
# %%
X.columns
# %%
X.head()

# %%

# %%
