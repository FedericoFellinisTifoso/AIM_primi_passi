# %% Import used packages
import pandas as pd
import numpy as np

from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

# %% Load the data
data_folder = "data/LWF/"
filename = "Schauer_LWF_results"
df_LWF = Load_Store_Data().loading(data_folder, filename, "parquet")

# %% Insert habitat type
df_LWF["habitat_type"] = "forest"
df_LWF["habitat_type"] = df_LWF["habitat_type"].astype("category", copy=False)

# %% Add timestamp to LFW 
df_LWF["timestamp"] = pd.NaT

# %% Add latitude and longitude to LFW 

df_LWF["latitude"] = [48.1198] * df_LWF.shape[0]
df_LWF["longitude"] = [11.937] * df_LWF.shape[0]

# df_LWF = geopandas.GeoDataFrame(df_LWF, 
#                                         geometry = geopandas.points_from_xy(df_LWF_temp_merged["longitude"], 
#                                                                             df_LWF_temp_merged["latitude"]))

df_LWF["latitude"] = df_LWF["latitude"].astype("category")
df_LWF["longitude"] = df_LWF["longitude"].astype("category")

# %% Use customer_sampleID as observation_id
df_LWF["dataset"] = "LFW"
df_LWF["dataset"] = df_LWF["dataset"].astype("category", copy=False)

df_LWF["observation_id"] = df_LWF["customer_sampleID"]

# %% Remove old target columns now that all data is extracted from them
df_LWF.drop(axis=1, columns=["customer_sampleID"], inplace = True)

# %% Store the cleaned data
data_folder = "data/LWF/"
filename = "LFW_a_posto"
Load_Store_Data().saving(df_LWF, data_folder, filename, "parquet")

# %%
X = pd.DataFrame(Load_Store_Data().loading(data_folder, filename, "parquet"))
# %%
X.columns
# %%
X.head()

# %%
X.info()
# %%
