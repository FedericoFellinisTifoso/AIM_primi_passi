# %% Load the packages
import pandas as pd
import numpy as np
import plotly.express as plx
import plotly.graph_objects as go
from plotly.subplots import make_subplots

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

from sklearn.cluster import KMeans
from sklearn.decomposition import TruncatedSVD

# %% Load the data
data_folder = "data/UnifiedData/"
filename = "complete_data_tutto"
#df_all =  Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "bin_data_tutto"
X_raw =  Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "target_data_tutto"
df_target =  Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "bold_bin_consenus_family_connection"
df_bin_consensus_family_connection =  Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "bold_bin_consenus_family_tutto"
df_bin_infos =  Load_Store_Data().loading(data_folder, filename, "parquet")

store_img_data_folder = "plots/FeaturesClustering/"

# %% Make one hot data
df_target["month"]
# %%

df_target[df_target["month"].dt.month_name() == "December"]
# %%
X_raw_dummy = X_raw.copy()
X_raw_dummy[X_raw > 0] = 1

X_plot = X_raw_dummy

# %%
mask = df_target["dataset"] == "LandKLIF"
data = X_plot[mask].values
# %%



# %%

# %%
#%%


# %%
kmeans = KMeans(n_clusters=5, random_state=0).fit(X_plot)

svd = TruncatedSVD(random_state=42)
X_svd = pd.DataFrame(svd.fit_transform(X_plot))

# %%
title_string = f"Feature Clustering in general"

fig = make_subplots(rows=1, cols=1)
for cluster in sorted(set(kmeans.labels_)):
    mask = kmeans.labels_ == cluster
    subplot = go.Scatter(x=X_svd.loc[mask,0], y = X_svd.loc[mask,1], 
                        name=str(cluster),
                        mode="markers"
                        )
    fig.add_trace(subplot, row = 1, col = 1)

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")

# %%

# %%

svd = TruncatedSVD(random_state=42)
X_svd = pd.DataFrame(svd.fit_transform(X_plot))

# %%
title_string = f"Feature Clustering along Habitat Type and Month"

#habitat_types = set(df_target["habitat_type"])
habitat_types=["agriculture", "forest", "nature", "urban"]
for i, habitat_type in enumerate(habitat_types):
    fig = make_subplots(rows=len(habitat_types), cols=1, row_titles = habitat_types)

    mask_habitat = df_target["habitat_type"]==habitat_type
    X_plot = X_raw_dummy[mask_habitat]
    kmeans = KMeans(n_clusters=2, random_state=0).fit(X_plot)
    
    for cluster in sorted(set(kmeans.labels_)):
        mask_cluster = kmeans.labels_ == cluster
        subplot = go.Scatter(x=X_svd.loc[mask_habitat,0][mask_cluster], y = X_svd.loc[mask_habitat,1][mask_cluster], 
                            name=str(cluster),
                            mode="markers"
                            )
        fig.add_trace(subplot, row = i + 1, col = 1)

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")

# %%
habitat_types
# %%

# %%
mask = df_target["dataset"] == "NatWald"
X_plot = X_raw_dummy[mask]

kmeans = KMeans(n_clusters=5, random_state=0).fit(X_plot)
kmeans.labels_

svd = TruncatedSVD(random_state=42)
X_svd = svd.fit_transform(X_plot)

title_string = f"Feature Clustering in general"

fig = plx.scatter(x = X_svd[:,0], y = X_svd[:,1],
    color = kmeans.labels_,
    hover_data={"dataset" : df_target.loc[mask, "dataset"], "observation_id": df_target.loc[mask ,"observation_id"]},
    labels = {"color" : "habiatat_type", "index" : "observation_id", "value" : "counts"},
    title = title_string
)

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")


# %% Check for features correlation

def histogram_intersection(a, b):

    v = np.minimum(a, b).sum().round(decimals=1)

    return v

# df = pd.DataFrame([(.2, .3), (.0, .6), (.6, .0), (.2, .1)],

#                   columns=['dogs', 'cats'])

# X_raw_dummy.corr(method=histogram_intersection)

# %%
mask = X_raw_dummy["AAB6107"] > 0
df_target[mask]
# %% 

bold_bin_uri_looked_for = {"AAB6107" : "Kleiner Heidegrashüpfer"}

title_string = f"Distribution of {list(bold_bin_uri_looked_for.keys())[0]} ({list(bold_bin_uri_looked_for.values())[0]})"

fig = plx.scatter(X_raw[list(bold_bin_uri_looked_for.keys())[0]],
    color = df_target["habitat_type"],
    hover_data={"dataset" : df_target["dataset"], "observation_id": df_target["observation_id"]},
    labels = {"color" : "habiatat_type", "index" : "observation_id", "value" : "counts"},
    title = title_string,
    opacity=1
)

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")

# %%
title_string = f"Significant Correlations of {list(bold_bin_uri_looked_for.keys())[0]} ({list(bold_bin_uri_looked_for.values())[0]}) with other BOLD BINs"

bold_bin_uri_corr = X_raw.apply(lambda x: x.corr(X_raw[list(bold_bin_uri_looked_for.keys())[0]]))
corr_threshold = 0.5
# %%
bold_bin_uri_corr_geq_threshold = list(bold_bin_uri_corr[bold_bin_uri_corr > 0.5].index)
X_plot = X_raw.loc[(X_raw[bold_bin_uri_corr_geq_threshold] != 0).any(axis=1), bold_bin_uri_corr_geq_threshold]

fig = make_subplots(rows=len(bold_bin_uri_corr_geq_threshold), cols=1)
for i, bold_bin in enumerate(bold_bin_uri_corr_geq_threshold):
    
    subplot = go.Scatter(x=X_plot[bold_bin].index,
                        y = X_plot[bold_bin],
                        name=f"{bold_bin}",
                        )
    #subplot.data[0].name= f"{dataset}_{habitat_type}"
    #fig.add_trace(subplot.data[0], row = 1, col = 1)
    fig.add_trace(subplot, row = i + 1, col = 1)

fig.update_layout(title_text=title_string)
Load_Store_Data().saving(fig, store_img_data_folder, title_string, "html")# %%

# %%




# %%
hex(13)
# %%
