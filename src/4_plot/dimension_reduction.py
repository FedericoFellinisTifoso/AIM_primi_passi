# %% Load the packages
import pandas as pd
import plotly.express as plx
import plotly.graph_objects as go

import sys
sys.path.append('../helper/')
from load_store_data import Load_Store_Data

# %% Load the data
data_folder = "data/UnifiedData/"
filename = "complete_data_tutto"
#df_all = Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "bin_data_tutto"
X_raw = Load_Store_Data().loading(data_folder, filename, "parquet")

filename = "target_data_tutto"
df_target = Load_Store_Data().loading(data_folder, filename, "parquet")

# %% Make one hot data
X_raw_dummy = X_raw.copy()
X_raw_dummy[X_raw > 0] = 1

data_folder = "/plots/DataSeparation/"


# %% Set dataset
mask_dataset = "LandKLIF"
mask = df_target["dataset"] == mask_dataset
mask &= df_target["habitat_type"].notna()
mask &= df_target["month"].notna()
X_raw_dummy = X_raw_dummy[mask]
df_target = df_target[mask]

# %% Dimensionality reduction
from sklearn.decomposition import TruncatedSVD

svd = TruncatedSVD(random_state=42)
X_svd = svd.fit_transform(X_raw_dummy)

# %%
from sklearn.decomposition import PCA

pca = PCA(n_components=2)
X_pca = pca.fit_transform(X_raw_dummy)


# %%
from sklearn.manifold import TSNE

tsne = TSNE(n_components=2, random_state=0,perplexity=50, n_iter=5000)
X_tsne = tsne.fit_transform(X_raw_dummy)


# %%
set(df_target["habitat_type"])
# %% Separation of the data

title_string = "Separatation of the Data into Months and Habitat Type"
fig = plx.scatter(x = X_svd[:,0], y = X_svd[:,1],
    facet_col = df_target["habitat_type"],
    color = df_target["month"].dt.month_name(),
    category_orders={"color": [date.month_name() for date in sorted(set(df_target["month"]))]},
    labels={"color": "month"},
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

data_folder = "/plots/DataSeparation/"

filename = "Separatation_of_the_Data_PCA_Truncated (LandKLIF)"
Load_Store_Data().saving(fig, data_folder, filename, "html")

# %%

fig = plx.scatter(x = X_pca[:,0], y = X_pca[:,1],
    facet_col = df_target["habitat_type"],
    color = df_target["month"].dt.month_name(),
    category_orders={"color": [date.month_name() for date in sorted(set(df_target["month"]))]},
    labels={"color": "month"},
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

filename = "Separatation_of_the_Data_PCA"
Load_Store_Data().saving(fig, data_folder, filename, "html")

# %%
fig = plx.scatter(x = X_tsne[:,0], y = X_tsne[:,1],
    facet_col = df_target["habitat_type"],
    color = df_target["month"].dt.month_name(),
    category_orders={"color": [date.month_name() for date in sorted(set(df_target["month"]))]},
    labels={"color": "month"},
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

filename = "Separatation_of_the_Data_TSNE"
Load_Store_Data().saving(fig, data_folder, filename, "html")

# %% Separation of the habitat types

title_string = "Separatation of the Habitat Types"
fig = plx.scatter(x = X_svd[:,0], y = X_svd[:,1],
    color = df_target["habitat_type"],
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

filename = "Separatation_of_the_Habitat_Types_PCA_Truncated"
filepath = os.path.expanduser(load_store_data.get_path(data_folder, filename, "html"))
fig.write_html(filepath)


fig = plx.scatter(x = X_pca[:,0], y = X_pca[:,1],
    color = df_target["habitat_type"],
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

filename = "Separatation_of_the_Habitat_Types_PCA"
filepath = os.path.expanduser(load_store_data.get_path(data_folder, filename, "html"))
fig.write_html(filepath)


fig = plx.scatter(x = X_tsne[:,0], y = X_tsne[:,1],
    color = df_target["habitat_type"],
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

filename = "Separatation_of_the_Habitat_Types_TSNE"
filepath = os.path.expanduser(load_store_data.get_path(data_folder, filename, "html"))
fig.write_html(filepath)


# %% Separation of the months

title_string = "Separatation of the Months"
fig = plx.scatter(x = X_svd[:,0], y = X_svd[:,1],
    color = df_target["month"].dt.month_name(),
    category_orders={"color": [date.month_name() for date in sorted(set(df_target["month"]))]},
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)


filename = "Separatation_of_the_Months_PCA_Truncated"
filepath = os.path.expanduser(load_store_data.get_path(data_folder, filename, "html"))
fig.write_html(filepath)


fig = plx.scatter(x = X_pca[:,0], y = X_pca[:,1],
    color = df_target["month"].dt.month_name(),
    category_orders={"color": [date.month_name() for date in sorted(set(df_target["month"]))]},
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

filename = "Separatation_of_the_Months_PCA"
filepath = os.path.expanduser(load_store_data.get_path(data_folder, filename, "html"))
fig.write_html(filepath)


fig = plx.scatter(x = X_tsne[:,0], y = X_tsne[:,1],
    color = df_target["month"].dt.month_name(),
    category_orders={"color": [date.month_name() for date in sorted(set(df_target["month"]))]},
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

filename = "Separatation_of_the_Months_TSNE"
filepath = os.path.expanduser(load_store_data.get_path(data_folder, filename, "html"))
fig.write_html(filepath)

# %% Separation of the Locations

title_string = "Separatation of the Latitude"
fig = plx.scatter(x = X_svd[:,0], y = X_svd[:,1],
    color = df_target["latitude"].round(0).astype("int").astype("string"),
    category_orders={"color": sorted(set(df_target["latitude"].round(0).astype("int").astype("string")))},
    labels={"color": "latitude"},
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

filename = "Separatation_of_the_Latitude_PCA_Truncated"
filepath = os.path.expanduser(load_store_data.get_path(data_folder, filename, "html"))
fig.write_html(filepath)


fig = plx.scatter(x = X_pca[:,0], y = X_pca[:,1],
    color = df_target["latitude"].round(0).astype("int").astype("string"),
    category_orders={"color": sorted(set(df_target["latitude"].round(0).astype("int").astype("string")))},
    labels={"color": "latitude"},
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

filename = "Separatation_of_the_Latitude_PCA"
filepath = os.path.expanduser(load_store_data.get_path(data_folder, filename, "html"))
fig.write_html(filepath)


fig = plx.scatter(x = X_tsne[:,0], y = X_tsne[:,1],
    color = df_target["latitude"].round(0).astype("int").astype("string"),
    category_orders={"color": sorted(set(df_target["latitude"].round(0).astype("int").astype("string")))},
    labels={"color": "latitude"},
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

filename = "Separatation_of_the_Latitude_TSNE"
filepath = os.path.expanduser(load_store_data.get_path(data_folder, filename, "html"))
fig.write_html(filepath)

# %%
# %% Separation of the datasets

title_string = "Separatation of the Datasets"
fig = plx.scatter(x = X_svd[:,0], y = X_svd[:,1],
    color = df_target["dataset"],
    title = title_string
)
fig.update_xaxes(visible=False)
fig.update_yaxes(visible=False)

filename = "Separatation_of_the_Datasets_PCA_Truncated"
Load_Store_Data().saving(fig, data_folder, filename, "html")


# %%
